import matplotlib.pyplot as plt
import numpy as np
# import quaternion
from numpy import linalg

import re
from mpl_toolkits.mplot3d import Axes3D
import scipy
from scipy import signal
from scipy.ndimage import gaussian_filter
import pandas as pd

def darr_dt(array, time):
    length = len(array)
    new_array = []
    a = 0
    for i in range(0, length-1, 1):
        a = array[i+1] - array[i]
        if a != 0:
            new_array.append((array[i+1] - array[i])/(0.006*(time[i+1] - time[i])))
        else:
            pass

    while len(new_array) < length:
        new_array.append(0)
    return new_array



def main(file_name, title_name):
    df = pd.read_csv(file_name)
    title_name = 'Илья до полёта вертолёта оптокинетика тест 2'
    n1 = 0
    n2 = -1
    hor = np.array(df['Hor_eye'][n1:n2])
    ver = np.array(df['Ver_eye'][n1:n2])
    time = np.array(df['Time'][n1:n2])
    # head_w = np.array(df['Head w'][:])
    # head_x = np.array(df['Head x'][:])
    # head_y = np.array(df['Head y'][:])
    # head_z = np.array(df['Head z'][:])
    # w_mod = np.array(df['Head_angular_vel'][:])

    # --------- убираем дрейф и сглаживаем ------------------------------------------------#
    hor = signal.detrend(hor)
    ver = signal.detrend(ver)
    n = 5
    hor = scipy.signal.medfilt(hor , kernel_size=n)
    ver = scipy.signal.medfilt(ver, kernel_size=n)
    n = 3
    hor = gaussian_filter(hor, sigma=n)
    ver = gaussian_filter(ver, sigma=n)

    # ---------- ищем угловые скорости зрачка  и КСВ -------------------------------------#
    d_hor = darr_dt(hor, time)
    d_ver = darr_dt(ver, time)
    k = 0
    for i in range(len(d_hor)):
        if abs(d_hor[i]) >= 4:
            k += 1
    coeff = (len(d_hor)-k)/len(d_hor)
    #
    # # -------------амплитуды и времена фаз ----------------------------------------------#
    flag = 0
    max_arr, min_arr = [], []
    max_index, min_index = [], []
    amount_min = -1
    for i in range(0, len(hor)-1, 1):
        if flag == 0:
            if hor[i+1] < hor[i]:
                max_arr.append(hor[i])
                max_index.append(i)
                flag = 1
            else:
                max_arr.append(None)
                min_arr.append(None)
                # pass
        if flag == 1:
            if hor[i+1] > hor[i]:
                min_arr.append(hor[i])
                min_index.append(i)
                amount_min += 1
                flag = 0
            else:
                min_arr.append(None)
                max_arr.append(None)
                # pass


    if max_index[0] < min_index[0]:
        # del max_arr[0]
        del max_index[0]

    if max_index[-1] > min_index[-1]:
        # del max_arr[-1]
        del max_index[-1]


    long_phase_amp, short_phase_amp = [], []
    short_phase_time, long_phase_time = [], []

    for i in range(len(max_index)):
        short_phase_amp.append(hor[max_index[i]] - hor[min_index[i]])
        long_phase_amp.append(hor[max_index[i]] - hor[min_index[i+1]])
        short_phase_time.append((time[max_index[i]] - time[min_index[i]])*0.006)
        long_phase_time.append((time[min_index[i+1]] - time[max_index[i]])*0.006)

    rate = len(max_index)/(0.006*(time[min_index[-1]] - time[min_index[0]]))

    # ------------------------ Вывод информации ----------------------------------------------#
    print(title_name)
    print("Количество нистагмов: ", len(max_index), "Частота нистагма", rate, "Гц")
    print("Коэффициент стабилизации взора: ", coeff*100, "%")
    print("Амплитуда длинной фазы ", round(np.mean(long_phase_amp), 3), "градуса. ",
          "Время длинной фазы ", round(np.mean(long_phase_time), 3),"секунд.")
    print("Амплитуда короткой фазы ", round(np.mean(short_phase_amp), 3), "градуса. ",
          "Время короткой фазы ", round(np.mean(short_phase_time), 3),"секунд.")

    fig = plt.figure(figsize=(10,10))
    plot_1 = fig.add_subplot(1,1,1)

    plot_1.plot(time*0.006, ver, '-', markersize = 1, color = "green", label = 'Вертикальное')
    plot_1.plot(time*0.006, hor, '-', markersize = 3, color = "orange", label = 'Горизонтальное')

    plot_1.set_title(title_name)
    plot_1.legend(loc="lower right", fontsize = 12)
    plot_1.grid()

    plt.show()
# ---------------------------------------------------------------------------------------#


main('/home/yana/OTO обработанные файлы/18.07.2021/Оптокинетика/Участки/Яна_после_верт_1.csv', 'Илья после вертолёта тест 1')

# n1 = 2800
# n2 = 3800
#
# data = []
# for i in range(n1, n2, 1):
#     data.append([hor[i], ver[i], head_w[i], head_x[i], head_y[i], head_z[i], time[i], w_mod[i]])
#
# columns = ['Hor_eye', 'Ver_eye', 'Head w', 'Head x', 'Head y', 'Head z', 'Time', 'Head_angular_vel']
#
# df = pd.DataFrame(data, columns=columns)
# df.to_csv(r'/home/yana/OTO обработанные файлы/18.07.2021/Оптокинетика/Участки/Виктор_до_верт_2.csv')

# ---------------------------------------------------------------------------------#


# fig = plt.figure(figsize=(10,10))
# plot_1 = fig.add_subplot(1,1,1)
# # plot_2 = fig.add_subplot(2,1,2)
#
# plot_1.plot(time, d_ver, '-', markersize = 1, color = "green", label = 'Вертикальное')
# plot_1.plot(time, d_hor, '-', markersize = 3, color = "orange", label = 'Горизонтальное')
#
# #
# # plot_1.plot(max_arr, 'o', markersize = 5, color = "blue")
# # plot_1.plot(min_arr, 'o', markersize = 5, color = "red")
# #
# #
# # plot_2.plot(d_hor, '.', markersize = 3, color = "orange", label = 'diff hor')
# # plot_2.plot(d_ver, '.', markersize = 3, color = "red", label = 'diff ver')
#
# plot_1.set_title(title_name)
# plot_1.legend(loc="lower right", fontsize = 12)
# plot_1.grid()
#
# # plot_2.set_title('')
# # plot_2.legend(loc="lower right", fontsize = 12)
# # plot_2.grid()
# #
# plt.show()
